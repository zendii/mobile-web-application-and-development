> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Assignment 1 Requirements:

1. Disributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions on PHP basics (Chs 1, 2)

#### Git commands w/short descriptions
 
1. git init - creates a new local respository
2. git status - outputs the status of files
3. git add - adds one or more files to staging
4. git commit - commit any files that were added with 'git add' 
5. git push - sends changes to the master branch 
6. git pull - fetch/merge changes on the remote server to the working directory
7. git diff - this command views merge conflicts with files

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost | Android Studio - My First App | java Hello*:

![Screenshot](img/screen.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zendii/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/zendii/myteamquotes/ "My Team Quotes Tutorial")


