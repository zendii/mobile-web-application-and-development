<?php
class Person{
    private $fname;
    private $lname;
    private $age;
    
    //parameterized constructor that accepts two arguments 
    public function __contruct($fn = "John", $ln="Doe", $ag=21){
        $this->fname=$fn;
        $this->lname=$ln;
        $this->age=$ag;
        echo("Creating <strong>" . $this->fname . " " .$this->lname . "is " . $this->age . "</strong> person object from parameterized contructor (accepts 3 agruments): <br/>");
    }
    
    //destructor method called as soon as there are no other references to a particular object
    function __destruct(){
        echo ("Destroying<strong>" . $this->fname . " " . $this->lname . "who is " . $this->age . "</strong> person object<br/>");
    }
    
    
    
    //setter methods
    public function SetFname($fn="Jane"){
        $this->fname=$fn;
    }
    
    public function SetLname($ln="Doe"){
        $this->lname=$ln;
    }
    
    public function SetAge($ag=21){
        $this->age=$ag;
    }
    
    
    //accessor methods
    public function GetFname(){
        return $this->fname;
    }
    
    public function GetLname(){
        return $this->lname;
    }
    
    public function GetAge(){
        return $this->age;
    }
}
?>
