# LIS4381 Mobile Web Application Development

## Sayvion Mayfield

### Assignment # Requirements:

[A1 README.md](a1/README.md)

* Disributed Version Control with Git and Bitbucket
* Development Installations
* Questions (Chs 1, 2)
* Provide screenshots of installations
* Provide git command descriptions
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)

[A2 README.md](a2/README.md)

* Developed a basic mobile application in Android Studio Created two activities 
* Linked a button on the main activity to the recipe activity
* Implemented a dark theme

[A3 README.md](a3/README.md):

* Created a database in MySQL with 3 entities.
* Applied business rules.
* Included 10 unique attributes and data for my entities.
* Successfully foward engineered my database.

[A4 README.md](a4/README.md)

* Developed a website portfolio with bootstrap.
* Added form controls to match attributes.
* Used regexp to only allow appropriate characters for each control.

[A5 README.md](a5/README.md):

* Developed a server side form data vaildation with PHP.
* Tested with data from MySQL ERDs and business rules (from assignment 3).
* Used regexp to only allow appropriate characters for each control.


### Project 1
[P1 README.md](p1/README.md)

* Created a launcher icon.
* Altered app background and other colors.
* Linked a button to launch a seperate activity.
* Added a border around image.
* Added a text shadow.


### Project 2
[P2 README.md](p2/README.md)

* Server-side validation with PHP.
* Implemented edit/delete function to records.
* Created a RSS Feed.

