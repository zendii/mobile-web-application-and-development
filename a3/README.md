> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Assignment 3 Requirements:

* Created a database in MySQL with 3 entities.
* Applied business rules.
* Included 10 unique attributes and data for my entities.
* Successfully foward engineered my database.
* Developed another app in Android Studio
* Practiced implementing... TextViews, EditText, Spinners, and etc... manually.
* Practiced if else statements within the app.

#### Assignment 3 MySQL database links:
* SQL File:

[SQL File](docs/a3.sql)

* MWB File:

[MWB File](docs/a3.mwb)

#### Assignment Screenshots:

*Screenshot of ERD model in MySQL:

![Screenshot](img/a3.png)


*Screenshot of my Artist App:

![Screenshot](img/event1.png)
![Screenshot](img/event2.png)
![Screenshot](img/event3.png)