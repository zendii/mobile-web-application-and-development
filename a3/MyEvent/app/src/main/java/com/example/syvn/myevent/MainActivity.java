package com.example.syvn.myevent;

import android.icu.text.NumberFormat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    double costPerTicket = 0.0, totalCost = 0;
    int numberOfTickets = 0;
    String groupchoice = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //shows app icon
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        final EditText tickets = (EditText)findViewById(R.id.txtTickets);
        final Spinner group = (Spinner)findViewById((R.id.txtGroup));
        Button cost = (Button)findViewById(R.id.btnCost);
        cost.setOnClickListener(new View.OnClickListener(){

            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v){
                groupchoice = group.getSelectedItem().toString();
                if (groupchoice.equals("J.Cole")){
                    costPerTicket=79.99;
                }
                else if (groupchoice.equals("Tyler, The Creator")){
                    costPerTicket=89.99;
                }
                else if (groupchoice.equals("6LACK")){
                    costPerTicket=69.99;
                }
                else {
                    costPerTicket=129.99;
                }
                numberOfTickets = Integer.parseInt(tickets.getText().toString());
                totalCost = costPerTicket * numberOfTickets;
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                result.setText("Cost of " + groupchoice + " is "+ nf.format(totalCost));


            }

        });



    }
}
