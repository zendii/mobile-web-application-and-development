> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Project 1 Requirements:


* Created a launcher icon.
* Altered app background and other colors.
* Linked a button to launch a seperate activity.
* Added a border around image.
* Added a text shadow.

#### Project Screenshots:



*Screenshot of my Business Card App:

![Screenshot](img/businesscard1.png)
![Screenshot](img/businesscard2.png)
