<!DOCTYPE html>
<html lang="en">

<head>
    <!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio includes tasks that involves development using PHP, MySQL, Java, HTML, CSS, and Javascript.">
    <meta name="author" content="Sayvion Mayfield">
    <link rel="icon" href="favicon.ico">

    <title>LIS 4381 - Project 1</title>

    <!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <!-- Bootstrap for responsive, mobile-first design. -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Note: following file is for form validation. -->
    <link rel="stylesheet" href="css/formValidation.min.css" />

    <!-- Starter template for your own custom styling. -->
    <link href="css/starter-template.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <style>
        .topad {
            border: none;
            padding-top: 90px;
            padding-right: 30px;
            padding-bottom: 50px;
            padding-left: 50px;
        }
        .imagepad2 {
            border: none;
            padding-top: 10px;
            padding-bottom: 20px;
        }

        .fa-check {
            color: green;
        }

        ol {
            list-style-type: decimal;
            margin: 0;
            padding: 0;
            overflow: hidden;
            text-align: left;
        }

        .img-float {

            float: left;
            width: 100%;
            height: 100%;

        }

        .padding{
            margin-top: 140px;
            
        }
    </style>
</head>

<body>

    <?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center topad">



                    <div class="page-header">
                        <h2>LIS4381</h2>
                        <h3>Project 1 Requirements:</h3>
                    </div>



                    <div>
                        <ul class="list-group ">
                            <li class="list-group-item justify-content-end col-6">
                                Created a launcher icon.

                                <span class="fa fa-check"></span>
                            </li>
                            <li class="list-group-item justify-content-between">
                                Altered app background and other colors.
                                <span class="fa fa-check"></span>
                            </li>
                            <li class="list-group-item justify-content-between">
                                Linked a button to launch a seperate activity.
                                <span class="fa fa-check"></span>
                            </li>
                             <li class="list-group-item justify-content-between">
                                Added a border around image.
                                <span class="fa fa-check"></span>
                            </li>
                            <li class="list-group-item justify-content-between">
                                Added a text shadow.
                                <span class="fa fa-check"></span>
                            </li>
                        </ul>
                    </div>
                   

                    
                    <div class="page-header">
                        <h4>Assignment Screenshots</h4>
                    </div>
                    <p>My Business Card Project Screenshots:</p>
                    <a href="img/businesscard1.png"><img src="img/businesscard1.png" class="rounded float-left img-float imagepad2" width="1200px" height="600px"></a> 
                    <a href="img/businesscard2.png"><img src="img/businesscard2.png" class="rounded float-left img-float imagepad2" width="1200px" height="600px"></a>
                     


                    </p>

                </div>
            </div>

            <?php include_once "../global/footer.php"; ?>

        </div>
        <!-- end starter-template -->
    </div>
    <!-- end container -->


    <!-- Bootstrap JavaScript
	================================================== -->
    <!-- Placed at end of document so pages load faster -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Turn off client-side validation, in order to test server-side validation.  -->
    <script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

    <!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
    <script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>



</body>

</html>
