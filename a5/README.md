> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Assignment 5 Requirements:

* Developed a server side form data vaildation with PHP.
* Tested with data from MySQL ERDs and business rules (from assignment 3).
* Used regexp to only allow appropriate characters for each control.

#### Assignment Screenshots:


*Screenshots of the server side form data valiadation:

![Screenshot](img/1.png)
![Screenshot](img/2.png)
