> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Assignment 4 Requirements:

* Developed a website portfolio with bootstrap.
* Added form controls to match attributes.
* Used regexp to only allow appropriate characters for each control.

#### Assignment Screenshots:

*Screenshot of my online portfolio:

![Screenshot](img/a4.png)


*Screenshot of the forms with data valiadation:

![Screenshot](img/form1.png)
![Screenshot](img/form2.png)
