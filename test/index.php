<!DOCTYPE html>
<html lang="en">

<head>
    <!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio includes tasks that involves development using PHP, MySQL, Java, HTML, CSS, and Javascript.">
    <meta name="author" content="Sayvion Mayfield">
    <link rel="icon" href="favicon.ico">

    <title>Skillset Testing</title>

    <!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <!-- Bootstrap for responsive, mobile-first design. -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Note: following file is for form validation. -->
    <link rel="stylesheet" href="css/formValidation.min.css" />

    <!-- Starter template for your own custom styling. -->
    <link href="css/starter-template.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <style>
        .topad {
            border: none;
            padding-top: 10px;
            padding-right: 30px;
            padding-bottom: 50px;
            padding-left: 50px;
        }
        .imagepad2 {
            border: none;
            padding-top: 10px;
            padding-bottom: 20px;
        }

        .fa-check {
            color: green;
        }

        ol {
            list-style-type: decimal;
            margin: 0;
            padding: 0;
            overflow: hidden;
            text-align: left;
        }

        .img-float {

            float: left;
            width: 100%;
            height: 100%;

        }

        .padding{
            margin-top: 140px;
            
        }
    </style>
</head>

<body>

    <?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center topad">



                    <div class="page-header">
                        <h2>LIS4381</h2>
                        <h3>Skillset Testing</h3>
                        
                               <h4> This page links to a few skillset projects/implementations.</h4>
                                
                    </div>



                   
                    <div class="page-header">
                        <span class="fa fa-calculator aria-hidden=true"></span>
                        <h4 >Simple <a href="../simpleCalculator/index.php">Calculator</a></h4>
                    </div>

                    <div>
                        <ul class="list-group ">
                            <li class="list-group-item justify-content-end col-6 text-success">
                                This skillset uses PHP for calculating and returning data.
                            </li>
                        </ul>
                    </div>
                    
                    
                    
                    <div class="page-header">
                        <span class="fa fa-users aria-hidden=true"></span>
                        <h4 >Simple Person <a href="../simplePersonClass/index.php">Class</a></h4>
                    </div>
                    <div>
                        <ul class="list-group ">
                            <li class="list-group-item justify-content-end col-6 text-danger">
                                Utlizing PHP to  blase with classes.
                            </li>
                        </ul>
                    </div>
                    
                    
                                        <div class="page-header">
                        <span class="fa fa-user aria-hidden=true"></span>
                        <h4 >Simple Person <a href="../simplePersonClassJava/index.php">Class</a> (Java version)</h4>
                    </div>
                    <div>
                        <ul class="list-group ">
                            <li class="list-group-item justify-content-end col-6 text-info">
                                Same as above but with pure Java blase.
                            </li>
                        </ul>
                    </div>
                    
                    <div class="page-header">
                        <span class="fa fa-rss aria-hidden=true"></span>
                        <h4 >Project 2's <a href="../p2/rssfeed.php">RSS Feed</a></h4>
                    </div>
                    <div>
                        <ul class="list-group ">
                            <li class="list-group-item justify-content-end col-6 text-success">
                                An RSS Feed
                            </li>
                        </ul>
                    </div>
                    
      
                    

                    
                </div>
            </div>

            <?php include_once "../global/footer.php"; ?>

        </div>
        <!-- end starter-template -->
    </div>
    <!-- end container -->


    <!-- Bootstrap JavaScript
	================================================== -->
    <!-- Placed at end of document so pages load faster -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Turn off client-side validation, in order to test server-side validation.  -->
    <script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

    <!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
    <script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>



</body>

</html>
