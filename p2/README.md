> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Project 2 Requirements:

* Server-side validation with PHP.
* Implemented edit/delete function to records.
* Created a RSS Feed.


#### Project Screenshots:


*Screenshots of the server side form data valiadation:

![Screenshot](img/1.png)
![Screenshot](img/2.png)
![Screenshot](img/3.png)

