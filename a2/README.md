> **NOTE:** This README.md file gives a description and useful information of this repository. **


# LIS4381

## Sayvion Mayfield

### Assignment 2 Requirements:

* Developed a basic mobile application in Android Studio Created two activities 
* Linked a button on the main activity to the recipe activity
* Implemented a dark theme

#### Assignment Screenshots:

*Screenshot of my sample Android Studio app running on a Nexus 6P Emulator:

![Screenshot](img/screen.png)
![Screenshot](img/screen2.png)






